# std-env

* Language: Java8

## How to Use
Please get the following jar files to add `pom.xml`.

* commons-collections-3.2.1
* hamcrest-core-1.3
* junit-4.12
* slf4j-api-1.6.6
* slf4j-jdk14-1.6.6



## Purpose

This is a sample environment to test the work of competitive programming.

When you use this source code, You don't need to cofuse the basic processing.

