package hackerrank;

import java.io.IOException;
import java.util.Scanner;

public class SolutionSherlock {

    // Complete the isValid function below.
    static String isValid(String s) {
        int length = s.length();
        String[] array = new String[length];
        int[] count = new int[length];
        for(int i=0;i<length; i++){
            array[i] = "";
        }
        int arrayLength = 0;
        for(int i=0; i<length; i++){
            String str = String.valueOf(s.charAt(i));
            boolean flag = true;
            for(int j=0; j<arrayLength; j++){
                if (str.equals(array[j])){
                    count[j]++;
                    flag = false;
                    break;
                }
            }
            if(flag){
                array[arrayLength] = str;
                count[arrayLength]++;
                arrayLength++;
            }
        }
        int num1 = 0;
        int num2 = 0;
        int count1 = 0;
        int count2 = 0;
        for(int i=0; i<length; i++){
            if(count[i]<=0){
                continue;
            }
            if(count[i]>0 && num1 == 0){
                num1 = count[i];
                count1++;
            }else if(count[i] == num1){
                count1++;
                if(count2>1){
                    return "NO";
                }
            }else if(num2==0){
                num2 = count[i];
                count2++;            	
            } else if(count[i] == num2){
                count2++;
                if(count1>1){
                    return "NO";
                }
            } else if(count[i] != num2){
                    return "NO";
            }
            
        }
        return "YES";
        
    }

    public static void main(String[] args) throws IOException {
        //BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
    	Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        String result = isValid(s);
        System.out.println(result);

//        bufferedWriter.write(result);
//        bufferedWriter.newLine();
//
//        bufferedWriter.close();

        scanner.close();
    }
}
