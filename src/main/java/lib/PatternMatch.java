package lib;

public class PatternMatch {

	private PatternMatch() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * 012-234-4567
	 */
	public static final String PHONE_NUMBER = "^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$";
	/**
	 * example@aaa.com
	 */
	public static final String MAIL_ADDRESS = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
	/**
	 * 123-4567
	 */
	public static final String ADDRESS = "^\\d{3}\\-\\d{4}$";
	/**
	 * https://www.google.co.jp/
	 */
	public static final String URL = "^(https?|ftp)(:\\/\\/[-_.!~*\\'()a-zA-Z0-9;\\/?:\\@&=+\\$,%#]+)$";
	/**
	 * Alphabetic characters(only small characters)
	 */
	public static final String  SMALL_ALPHABET = "^[a-z]+$";
	/**
	 * Alphabetic characters(only large characters)
	 */
	public static final String  LARGE_ALPHABET = "^[A-Z]+$";
	/**
	 * Alphabetic characters(only large characters)
	 */
	public static final String  ALPHABET = "^[a-zA-Z]+$";
	/**
	 * Alphabetic characters(plus number)
	 */
	public static final String  ALPHABET_AND_NUMBER = "^[\\w]+$";
	/**
	 * IPv4 address
	 */
	public static final String IPV4_ADDRESS  = "^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3} ([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
	/**
	 * IPv6 address
	 */
	public static final String IPV6_ADDRESS  = "(::(ffff:([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.([0-9]|[1-9][0-9]|"
				+ "1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}|(([0-9a-f]|[1-9a-f][0-9a-f]{1,3})(:([0-9a-f"
				+ "]|[1-9a-f][0-9a-f]{1,3})){0,5})?)|([0-9a-f]|[1-9a-f][0-9a-f]{1,3})(::(([0-9a-f]|"
				+ "[1-9a-f][0-9a-f]{1,3})(:([0-9a-f]|[1-9a-f][0-9a-f]{1,3})){0,4})?|:([0-9a-f]|[1-9"
				+ "a-f][0-9a-f]{1,3})(::(([0-9a-f]|[1-9a-f][0-9a-f]{1,3})(:([0-9a-f]|[1-9a-f][0-9a-"
				+ "f]{1,3})){0,3})?|:([0-9a-f]|[1-9a-f][0-9a-f]{1,3})(::(([0-9a-f]|[1-9a-f][0-9a-f]"
				+ "{1,3})(:([0-9a-f]|[1-9a-f][0-9a-f]{1,3})){0,2})?|:([0-9a-f]|[1-9a-f][0-9a-f]{1,3"
				+ "})(::(([0-9a-f]|[1-9a-f][0-9a-f]{1,3})(:([0-9a-f]|[1-9a-f][0-9a-f]{1,3}))?)?|:(["
				+ "0-9a-f]|[1-9a-f][0-9a-f]{1,3})(::([0-9a-f]|[1-9a-f][0-9a-f]{1,3})?|(:([0-9a-f]|["
				+ "1-9a-f][0-9a-f]{1,3})){3}))))))";
	
	
	/**
	 * This method changes first string to large character
	 * ex) sample -> Sample
	 * @param str
	 * @return
	 */
	public static String upperCaseFirst(String str) {
		String ret = str.toUpperCase().substring(0, 1) + str.toLowerCase().substring(1);
		return ret;
	}
}
