package math.impl;

import math.spec.DifferentialEquation;
import math.spec.Factory;
import math.spec.FunctionFactory;

/**
 * オイラー法（常微分方程式を近似的に解くアルゴリズム）
 */
public class Euler{
	
	/**
	 * 
	 * @param x0 初期値
	 * @param t0 区間 [t0, tn]
	 * @param tn 
	 * @param n 分割数
	 * @return
	 */
	public double calc(double x0, double t0, double tn, int n) {
		Factory factory = new FunctionFactory();
		DifferentialEquation differentialEquation = factory.create("RCCircuit");
		int i;
		double x, t, h;
		x = x0;
		t = t0;
		h = (tn - t0) / n;

		// calculate recursion.
		for (i = 1; i <= n; i++) {
			x += differentialEquation.dxdt(x) * h;
			t = t0 + i * h;
			System.out.printf("x(%f)=%f\n", t, x);
		}
		return x;
	}
}
