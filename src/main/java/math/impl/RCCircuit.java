package math.impl;

import math.spec.DifferentialEquation;

public class RCCircuit extends DifferentialEquation {

    private String owner;

    public RCCircuit() {}
    
    public RCCircuit(String owner) {
        System.out.println("Execute Function: " + owner);
        this.owner = owner;
    }
	
	/**
	 * 
	 * Function detail:
	 *  dv(t)/dt = (e-v(t))/rc
	 * @see math.spec.DifferentialEquation#dxdt(double)
	 */
	@Override
	public double dxdt(double x) {
		double c = 0.001;
		double r = 100;
		double e = 10;
		return (e - x) / r / c;
	}

	public String getName(){
		return owner;
	}
}
