package math.spec;

public abstract class DifferentialEquation {

	public abstract double dxdt(double x);
}
