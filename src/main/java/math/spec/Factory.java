package math.spec;


public abstract class Factory {
	
	   public final DifferentialEquation create(String owner) {
	        DifferentialEquation DifferentialEquation = createDifferentialEquation(owner);
	       // registerProduct(DifferentialEquation);
	        return DifferentialEquation;
	    }

	    protected abstract DifferentialEquation createDifferentialEquation(String owner);
	    //protected abstract void registerProduct(DifferentialEquation product);
}
