package math.spec;

import math.impl.RCCircuit;


public class FunctionFactory extends Factory{
	
   // private List owners = new ArrayList();

    protected DifferentialEquation createDifferentialEquation(String owner) {
    	/**
    	 * 
    	 * 計算クラスを作って、ここを書き換えるだけで計算式を変更できる。
    	 */
        return new RCCircuit(owner);
    }

//    protected void registerProduct(DifferentialEquation product) {
//        owners.add(((Euler)product).getOwner() );
//    }
//
//    public List getOwners() {
//        return owners;
//    }
}
