package atcoder;

import static org.junit.Assert.fail;
import lib.StandardInputSnatcher;
import lib.StandardOutputSnatcher;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @see <a href="https://practice.contest.atcoder.jp/tasks/practice_1">AtCoder</a>
 * @author shoichiro
 *
 */
public class MainTest {
	
	private StandardOutputSnatcher out = new StandardOutputSnatcher();
    private StandardInputSnatcher in = new StandardInputSnatcher();

    @Before
    public void before() {
    	System.setOut(out);
        System.setIn(in);
    }

    @After
    public void after() {
    	System.setOut(null);
        System.setIn(null);
    }

    @Test
    public void test1() {
        in.inputln("1");
        in.inputln("2 3");
        in.inputln("test");

        try {
            Main.main(null);
            Assert.assertThat(out.readLine(), Is.is("6 test"));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }
    
}
