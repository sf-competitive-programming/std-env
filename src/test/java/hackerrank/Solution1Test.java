package hackerrank;

import static org.junit.Assert.fail;
import lib.StandardInputSnatcher;
import lib.StandardOutputSnatcher;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Solution1Test {

	private StandardOutputSnatcher out = new StandardOutputSnatcher();
    private StandardInputSnatcher in = new StandardInputSnatcher();
    
    @Before
    public void before() {
    	System.setOut(out);
        System.setIn(in);
    }

    @After
    public void after() {
    	System.setOut(null);
        System.setIn(null);
    }

    @Test
    public void test1() {
        in.inputln("2,3,4,5,6");

        try {
            Solution1.main(null);
            Assert.assertThat(out.readLine(), Is.is("2,3,4,5,6"));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }

    @Test
    public void test2() {
        in.inputln("abcdef");

        try {
            Solution1.main(null);
            Assert.assertThat(out.readLine(), Is.is("abcdef"));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }
    
    @Test
    public void test3() {
        in.inputln("*P+()'~(+%&\"&%\"P>+P\"");

        try {
            Solution1.main(null);
            Assert.assertThat(out.readLine(), Is.is("*P+()'~(+%&\"&%\"P>+P\""));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }
}
