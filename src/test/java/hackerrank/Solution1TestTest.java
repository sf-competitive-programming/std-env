package hackerrank;

import org.junit.Test;

public class Solution1TestTest {
	/**
	 * Testing Test Class on Test Method.
	 * We usually use TestSuite library to test class level.
	 * When you test by method level, let's use this.
	 */
	@Test
	public void Solution1Test(){
		Solution1Test testClass = new Solution1Test();
		testClass.before();
		testClass.test1();
		testClass.after();
	}

}
