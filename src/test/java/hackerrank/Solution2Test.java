package hackerrank;

import static org.junit.Assert.fail;
import lib.StandardInputSnatcher;
import lib.StandardOutputSnatcher;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Solution2Test {
	
	private StandardOutputSnatcher out = new StandardOutputSnatcher();
    private StandardInputSnatcher in = new StandardInputSnatcher();

    @Before
    public void before() {
    	System.setOut(out);
        System.setIn(in);
    }

    @After
    public void after() {
    	System.setOut(null);
        System.setIn(null);
    }

    @Test
    public void test1() {
        in.inputln("I.am a student.");

        try {
            Solution2.main(null);
            Assert.assertThat(out.readLine(), Is.is("I.am a student."));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }
    
    @Test
    public void test2() {
        in.inputln("I.am a student.You are a student.2000");

        try {
            Solution2.main(null);
            Assert.assertThat(out.readLine(), Is.is("I.am a student.You are a student.2000"));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }
}
