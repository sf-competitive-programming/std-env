package hackerrank;

import static org.junit.Assert.fail;
import lib.StandardInputSnatcher;
import lib.StandardOutputSnatcher;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SolutionSherlockTest {
	private StandardOutputSnatcher out = new StandardOutputSnatcher();
    private StandardInputSnatcher in = new StandardInputSnatcher();

    @Before
    public void before() {
    	System.setOut(out);
        System.setIn(in);
    }

    @After
    public void after() {
    	System.setOut(null);
        System.setIn(null);
    }

    @Test
    public void test1() {
        in.inputln("aabbcd");

        try {
            SolutionSherlock.main(null);
            Assert.assertThat(out.readLine(), Is.is("NO"));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }
    
    @Test
    public void test2() {
        in.inputln("aaabbbcccdddee");

        try {
            SolutionSherlock.main(null);
            Assert.assertThat(out.readLine(), Is.is("YES"));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }

    @Test
    public void test3() {
        in.inputln("abcdefghhgfedecba");

        try {
            SolutionSherlock.main(null);
            Assert.assertThat(out.readLine(), Is.is("YES"));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }

    @Test
    public void test4() {
        in.inputln("aaaaabc");

        try {
            SolutionSherlock.main(null);
            Assert.assertThat(out.readLine(), Is.is("YES"));
     
        } catch (Exception e) {
        	fail("Exception.");
        }
    }
}
