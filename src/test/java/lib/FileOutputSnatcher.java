package lib;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import junit.framework.TestCase;

public class FileOutputSnatcher extends TestCase{

	private File fileMS932 = new File("testFileWriteMS932");
	private File fileUTF8 = new File("testFileWriteUTF8");
	private File fileDefEncode = new File("testFileWriteDefEncode");
	private static final String ENCODE_MS932 = "MS932";
	private static final String ENCODE_UTF8 = "UTF8";
	private static final String ENCODE_DEFAULT = System.getProperty("file.encoding");


	protected void setUp() throws Exception {
		super.setUp();
		setUpFile(fileMS932, ENCODE_MS932);
		setUpFile(fileUTF8, ENCODE_UTF8);
		setUpFile(fileDefEncode, ENCODE_DEFAULT);
	}

	protected void tearDown() throws Exception {
		tearDownFile(fileMS932);
		tearDownFile(fileUTF8);
		tearDownFile(fileDefEncode);
		super.tearDown();
	}

	private void tearDownFile(File file){
		if(!file.exists()){
			return;
		}
		if(!file.delete()){
			file.deleteOnExit();
		}
	}
	
	private void setUpFile(File file,String encStr){
		if(file.exists()){
			file.delete();
		}
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),encStr));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			ResourceCloseCommon.close(bw);
		}
	}

}
