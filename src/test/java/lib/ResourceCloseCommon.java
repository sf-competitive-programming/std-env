package lib;

import hackerrank.Solution1Test;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.Channel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.Test;

/**
 * Closing Resource Library Don't create instance!!
 */
public class ResourceCloseCommon {

	public ResourceCloseCommon() {
	}

	/**
	 * Closing PreparedStatement
	 * 
	 * @param ps
	 */
	public static void close(Statement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing Connection
	 * 
	 * @param con
	 */
	public static void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing ResultSet
	 * 
	 * @param rs
	 */
	public static void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing PreparedStatement and ResultSet
	 * 
	 * @param ps
	 * @param rs
	 */
	public static void close(Statement ps, ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (ps != null) {
			try {
				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing Connection and PreparedStatement
	 * 
	 * @param con
	 * @param ps
	 */
	public static void close(Connection con, Statement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (con != null) {
			try {
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing Connection and PreparedStatement&ResultSet
	 * 
	 * @param con
	 * @param ps
	 * @param rs
	 */
	public static void close(Connection con, Statement ps, ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (ps != null) {
			try {
				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (con != null) {
			try {
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing InputStream
	 * 
	 * @param inputStream
	 */
	public static void close(InputStream inputStream) {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing Reader
	 * 
	 * @param rd
	 */
	public static void close(Reader rd) {
		if (rd != null) {
			try {
				rd.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing OutputStream
	 * 
	 * @param outputStream
	 */
	public static void close(OutputStream outputStream) {
		if (outputStream != null) {
			try {
				outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing Writer
	 * 
	 * @param writer
	 */
	public static void close(Writer writer) {
		if (writer != null) {
			try {
				writer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing ServerSocket
	 * 
	 * @param serverSocket
	 */
	public static void close(ServerSocket serverSocket) {
		if (serverSocket != null) {
			try {
				serverSocket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing Socket
	 * 
	 * @param socket
	 */
	public static void close(Socket socket) {
		if (socket != null) {
			try {
				socket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing Channel
	 * 
	 * @param ch
	 */
	public static void close(Channel ch) {
		if (ch != null) {
			try {
				ch.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
