package math.impl;

import math.impl.Euler;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

public class EulerTest {

	
	@Test
	public void normalCase(){
        Euler euler = new Euler();
        double xn = euler.calc(0, 0, 1, 100);

        double expected = 9.999734386011124;
        Assert.assertThat(Math.round(xn), Is.is(Math.round(expected)));
	}
}
