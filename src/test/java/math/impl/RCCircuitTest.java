package math.impl;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

public class RCCircuitTest {

	@Test
	public void normalCase0(){
		RCCircuit rcCircuit = new RCCircuit();
        double xn = rcCircuit.dxdt(0);

        double expected = 100L;
        Assert.assertThat(Math.round(xn), Is.is(Math.round(expected)));
	}

	@Test
	public void normalCase5(){
		RCCircuit rcCircuit = new RCCircuit();
        double xn = rcCircuit.dxdt(5);

        double expected = 50L;
        Assert.assertThat(Math.round(xn), Is.is(Math.round(expected)));
	}

	
	@Test
	public void normalCase10(){
		RCCircuit rcCircuit = new RCCircuit();
        double xn = rcCircuit.dxdt(10);

        double expected = 0L;
        Assert.assertThat(Math.round(xn), Is.is(Math.round(expected)));
	}

	
}
